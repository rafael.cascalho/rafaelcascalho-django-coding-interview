from django.db import models

from subjects.models import Subject


# IMPL: rule of 7 subjects
class Student(models.Model):
    enrollment_complete = models.BooleanField(default=False)
    subjects = models.ManyToManyField(Subject, verbose_name='subjects')
